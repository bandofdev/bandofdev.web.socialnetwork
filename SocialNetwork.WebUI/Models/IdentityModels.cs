﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace SocialNetwork.WebUI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Age { get; set; }
        public DateTime? Birthday { get; set; }
        public int Phone { get; set; }
        public string Town { get; set; }
        public string School { get; set; }
        public string University { get; set; }
        public string Carier { get; set; }
        public string Interests { get; set; }
        public string About { get; set; }
        //public DateTime DataCreateAccount = DateTime.Now; //rework this 

        //public ApplicationUserProfile Profile { get; set; }

        public ICollection<Chat> Chats { get; set; }
        public ICollection<Note> Notes { get; set; }

        public ApplicationUser()
        {
            Chats = new List<Chat>();
            Notes = new List<Note>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class Chat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DataCreation { get; set; }

        public ICollection<ApplicationUser> Users { get; set; }
        public ICollection<Message> Messages { get; set; }

        public Chat()
        {
            Users = new List<ApplicationUser>();
            Messages = new List<Message>();
        }
    }

    public class Message
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Body { get; set; }
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }

        public int? ChatId { get; set; }
        public Chat Chat { get; set; }
    }

    public class Note
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }

        public int? ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Note> Notes { get; set; }
    }
}